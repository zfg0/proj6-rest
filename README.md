**Author:** Zane Globus-O'Harra

**Contact:** zfg@uoregon.edu

### Project Description

Application calculates the control times for a brevet. This is a type of bicycling event that
follows [these rules](https://rusa.org/pages/acp-brevet-control-times-calculator). The application
also includes options to submit and display the calculated control times. "Submit" will send
the inputted data to a database, and "Display" will return the database data to the screen.

### Operation

- In a linux shell, navigate to /DockerRestAPI and run `docker-compose build up` to build and
run the application

- Access the app via `<host:port>` in your browser (as currently configured, the application
will run on port 5550).

- Access a specified service with `<host:port>/<service>` where `<service>` is replaced with
a service described on port 5553 as currently configured.

- If you would like to display a certain number of results, modify the url with `?top=<num>` where `<num>`
is a whole positive number. 

### Services

* `http://<host:port>/listAll` should return all open and close times in the database
* `http://<host:port>/listOpenOnly` should return open times only
* `http://<host:port>/listCloseOnly` should return close times only

* `http://<host:port>/listAll/csv` should return all open and close times in CSV format
* `http://<host:port>/listOpenOnly/csv` should return open times only in CSV format
* `http://<host:port>/listCloseOnly/csv` should return close times only in CSV format

* `http://<host:port>/listAll/json` should return all open and close times in JSON format
* `http://<host:port>/listOpenOnly/json` should return open times only in JSON format
* `http://<host:port>/listCloseOnly/json` should return close times only in JSON format

* `http://<host:port>/listOpenOnly/csv?top=3` should return top 3 open times only (in ascending order) in CSV format 
* `http://<host:port>/listOpenOnly/json?top=5` should return top 5 open times only (in ascending order) in JSON format
* `http://<host:port>/listCloseOnly/csv?top=6` should return top 5 close times only (in ascending order) in CSV format
* `http://<host:port>/listCloseOnly/json?top=4` should return top 4 close times only (in ascending order) in JSON format