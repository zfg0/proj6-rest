<html>
    <head>
        <title>CIS 322 REST-api</title>
    </head>

    <body>
        <h1>listAll</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll');
            $obj = json_decode($json);
	            $times = $obj->result;

            if ($times == []){
                echo "Database is empty"; 
            } else{
                foreach($times as $k) {
                    $open_time = $k->open_time;
                    $close_time = $k->close_time;
                    $km = $k->km;
                    echo "<li>Open:$open_time</li><li>Close:$close_time</li><li>Distance:$km</li>";

                }
            }
            ?>
        </ul>
        
        <!-- ######### -->
        <h1>listOpenOnly</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listOpenOnly');
            $obj = json_decode($json);
	            $times = $obj->result;

            if ($times == []){
                echo "Database is empty"; 
            } else{
                foreach($times as $k) {
                    $open_time = $k->open_time;
                    $km = $k->km;
                    echo "<li>Open:$open_time</li><li>Distance:$km</li>";

                }
            }
            ?>
        </ul>
        
        <!-- ######### -->
        <h1>listCloseOnly</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listCloseOnly');
            $obj = json_decode($json);
	            $times = $obj->result;

            if ($times == []){
                echo "Database is empty"; 
            } else{
                foreach($times as $k) {
                    $close_time = $k->close_time;
                    $km = $k->km;
                    echo "<li>Close:$close_time</li><li>Distance:$km</li>";

                }
            }
            ?>
        </ul>

        <!-- 
            How do i add functionality to get top `k` times??????
        -->
        
        <!-- ######### -->
        <h1>/listAllCsv</h1>
        <ul>
            <?php
            $entry = file_get_contents('http://laptop-service/listAll/csv');
            echo nl2br($entry);
            ?>
        </ul>

        <!-- ######### -->
        <h1>/listOpenOnlyCsv</h1>
        <ul>
            <?php
            $data = file_get_contents('http://laptop-service/listOpenOnly/csv');
            echo nl2br($data);
            ?>
        </ul>

        <!-- ######### -->
        <h1>/listClosedOnlyCsv</h1>
        <ul>
            <?php
            $data = file_get_contents('http://laptop-service/listCloseOnly/csv');
            echo nl2br($data);
            ?>
        </ul>
    </body>
</html>
