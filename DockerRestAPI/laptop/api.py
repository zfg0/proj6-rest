"""
Used to listAll, listOpenOnly, etc...

Also converts csv to json and vice versa
"""

import flask
from flask import Flask, request, Response
from flask_restful import Resource, Api, reqparse
import json, csv
import pymongo
from pymongo import MongoClient
import logging

# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient('db', 27017)
db = client.tododb
collection = db.control

# class Laptop(Resource):
#     def get(self):
#         return {
#             'Laptops': ['Mac OS', 'Dell',
#             'Windozzee',
# 	    'Yet another laptop!',
# 	    'Yet yet another laptop!'
#             ]
#         }


def get_top():
    """simple helper func to get 'top' argument"""
    return request.args.get('top', 0, type=int)


def get_data(top, bool_open, bool_close, sort_by=None):
    """get data from DB in json string format"""
    maximum = 40
    auto_sort_by = "open_time"
    if top != None:
        maximum = top
    if sort_by != None:
        auto_sort_by = sort_by

    times = collection.find().sort(auto_sort_by, pymongo.ASCENDING).limit(int(maximum))
    result = []

    for elem in times:
        if bool_open and bool_close:
            to_append = {"km": elem["km"],
                     "open_time": elem["open_time"],
                     "close_time": elem["close_time"]
                     }
            result.append(to_append)
        elif bool_open:
            to_append = {"km": elem["km"],
                     "open_time": elem["open_time"]
                     }
            result.append(to_append)
        else:
            to_append = {"km": elem["km"],
                     "close_time": elem["close_time"]
                     }
            result.append(to_append)

    return result


def convert_to_csv(json_data, bool_open, bool_close):
    """converts json string to csv

    Reference: https://www.geeksforgeeks.org/convert-json-to-csv-in-python/
    """
    data = json_data
    csv_file = open("data.csv", "w")
    to_write = csv.writer(csv_file)

    if bool_open and bool_close:
        to_write.writerow(["km", "open_time", "close_time"])
        for row in data:
            to_write.writerow([row["km"],
                               row["open_time"],
                               row["close_time"]
                               ]
                              )
    elif bool_open:
        to_write.writerow(["km", "open_time"])
        for row in data:
            to_write.writerow([row["km"],
                               row["open_time"]
                               ]
                              )
    else:
        to_write.writerow(["km", "close_time"])
        for row in data:
            to_write.writerow([row["km"],
                               row["close_time"]
                               ]
                              )
    return


class listAll(Resource):

    def get(self, top=None):
        top = get_top()
        if top != 0:
            result = get_data(top, True, True)
        else:
            result = get_data(None, True, False)

        return flask.jsonify(result=result)


class listOpenOnly(Resource):

    def get(self):
        top = get_top()
        if top != 0:
            result = get_data(top, True, False, sort_by="open_time")
        else:
            result = get_data(None, True, False, sort_by="open_time")

        return flask.jsonify(result=result)


class listClosedOnly(Resource):

    def get(self):
        top = get_top()
        if top != 0:
            result = get_data(top, True, False, sort_by="close_time")
        else:
            result = get_data(None, True, False, sort_by="close_time")

        return flask.jsonify(result=result)


class listAllCSV(Resource):

    def get(self):
        top = get_top()
        if top != 0:
            result = get_data(top, True, True)
        else:
            result = get_data(top, True, True)

        convert_to_csv(result, True, True)
        csv_file = open("data.csv", mode='r')

        return Response(csv_file, mimetype="text/csv")


class listOpenOnlyCSV(Resource):

    def get(self):
        top = get_top()
        if top != 0:
            result = get_data(top, True, False, sort_by="open_time")
        else:
            result = get_data(None, True, False, sort_by="open_time")

        convert_to_csv(result, True, False)
        csv_file = open("data.csv", mode='r')

        return Response(csv_file, mimetype="text/csv")


class listCloseOnlyCSV(Resource):

    def get(self):
        top = get_top()
        if top != 0:
            result = get_data(top, True, False, sort_by="close_time")
        else:
            result = get_data(None, True, False, sort_by="close_time")

        convert_to_csv(result, False, True)
        csv_file = open("data.csv", mode='r')

        return Response(csv_file, mimetype="text/csv")


# Create routes
# Another way, without decorators
# api.add_resource(Laptop, '/')
api.add_resource(listAll,"/listAll","/listAll/json")
api.add_resource(listOpenOnly, "/listOpenOnly","/listOpenOnly/json")
api.add_resource(listClosedOnly, "/listCloseOnly","/listCLoseOnly/json")
api.add_resource(listAllCSV, "/listAll/csv")
api.add_resource(listOpenOnlyCSV, "/listOpenOnly/csv")
api.add_resource(listCloseOnlyCSV, "/listCloseOnly/csv")


# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
