"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask  # ModuleNotFound Error????
from flask import Flask, redirect, request, url_for, render_template, g
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging

###
# Globals
###

# flask app init
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

# mongo client init
client = MongoClient("db", 27017)
db = client.tododb
db_collection = db.control

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    distance = request.args.get('distance', 0, type=float)
    date = request.args.get('date', 0, type=str)
    time = request.args.get('time', 0, type=str)

    notice = ""

    if km < 0:
        notice = "control distance must be >=0"
        km = 0

    if km > (distance * 1.2):
        notice = "control can't be more than 120% brevet distance"

    start_time = date + " " + time + ":00"
    start_time_arrow = arrow.get(start_time, "YYY-MM-DD HH:mm:ss")

    open_time = acp_times.open_time(km,
                                    distance,
                                    start_time_arrow.isoformat())
    close_time = acp_times.close_time(km,
                                      distance,
                                      start_time_arrow.isoformat())
    result = {"open": open_time, "close": close_time, "notice": notice}
    return flask.jsonify(result=result)


@app.route("/display")
def display():
    """display times from DB"""
    app.logger.debug("displaying times from DB...")

    # `g` lives in the request context, i.e., created afresh when
    # the requests starts, and available until it ends
    # https://stackoverflow.com/questions/15083967/when-should-flask-g-be-used
    flask.g.km = []
    flask.g.open_times = []
    flask.g.close_times = []

    for element in db_collection.find():
        flask.g.km.append(element["km"])
        flask.g.open_times.append(element["open_time"])
        flask.g.close_times.append(element["close_time"])

    return flask.render_template("display_times.html")


@app.route('/submit', methods = ['POST'])
def submit():
    """add a current data to the DB"""
    km_list = request.form.getlist("km")
    open_time_list = request.form.getlist("open_times")
    close_time_list = request.form.getlist("close_times")

    if not km_list:
        # no data submitted; redirect to empty form
        return flask.redirect(flask.url_for("index"))

    for i in range(40):
        if km_list[i] == "":
            # if no data to add, do nothing
            continue
        result = {"km": km_list[i],
                  "open_time": open_time_list[i],
                  "close_time": close_time_list[i]}
        # create a result and add it to the DB
        db_collection.insert_one(result)

    # data submitted; redirect to empty form
    return flask.redirect(flask.url_for("index"))


app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
